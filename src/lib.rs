use std::{collections::VecDeque, io::Read};

#[derive(Debug)]
pub enum Op {
    Inc,
    Dec,
    Right,
    Left,
    Inp,
    Out,
    Loop(Vec<Op>),
}

#[derive(Debug)]
pub struct State {
    pub ip: usize,
    pub memory: VecDeque<u8>,
}

impl State {
    pub fn new() -> Self {
        State {
            ip: 0,
            memory: VecDeque::new(),
        }
    }

    pub fn right(&mut self) {
        self.ip += 1;
    }
    pub fn left(&mut self) {
        if self.ip == 0 {
            self.memory.push_front(0);
        } else {
            self.ip -= 1;
        }
    }

    pub fn inc(&mut self) {
        let nv = self.curr_mut().wrapping_add(1);
        *self.curr_mut() = nv;
    }

    pub fn dec(&mut self) {
        let nv = self.curr_mut().wrapping_sub(1);
        *self.curr_mut() = nv;
    }

    pub fn curr_mut(&mut self) -> &mut u8 {
        if self.ip >= self.memory.len() {
            self.memory.resize(self.ip + 1, 0);
        }
        self.memory.get_mut(self.ip).unwrap()
    }

    pub fn out(&mut self) {
        print!("{}", *self.curr_mut() as char);
    }

    pub fn inp(&mut self) {
        let mut buf = [0];
        std::io::stdin().read_exact(&mut buf).unwrap();
        *self.curr_mut() = buf[0];
    }

    pub fn exec(&mut self, op: &Op) {
        match op {
            Op::Inc => self.inc(),
            Op::Dec => self.dec(),
            Op::Right => self.right(),
            Op::Left => self.left(),
            Op::Inp => self.inp(),
            Op::Out => self.out(),
            Op::Loop(sub_ops) => {
                while *self.curr_mut() != 0 {
                    for op in sub_ops {
                        self.exec(op);
                    }
                }
            }
        }
    }
}

pub fn parse(program: String) -> Vec<Op> {
    let mut ops = vec![];
    let mut stack: Vec<Vec<Op>> = vec![];

    for c in program.chars() {
        let op = match c {
            '>' => Op::Right,
            '<' => Op::Left,
            '+' => Op::Inc,
            '-' => Op::Dec,
            '.' => Op::Out,
            ',' => Op::Inp,
            ']' => Op::Loop(stack.pop().unwrap()),
            '[' => {
                stack.push(vec![]);
                continue;
            }
            _ => continue,
        };

        if stack.is_empty() {
            ops.push(op);
        } else {
            stack.last_mut().unwrap().push(op);
        }
    }

    ops
}
