use brainfuck_rs::{parse, State};

fn main() -> Result<(), std::io::Error> {
    let program = std::fs::read_to_string(
        std::env::args()
            .nth(1)
            .unwrap_or("helloworld.bf".to_string()),
    )?;

    let mut state = State::new();
    for op in parse(program) {
        state.exec(&op);
    }

    Ok(())
}
